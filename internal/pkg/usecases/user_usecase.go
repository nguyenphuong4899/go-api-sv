package usecases

import (
	"go-api-sv/internal/pkg/domains/interfaces"
	"go-api-sv/internal/pkg/domains/models/entities"
	"go-api-sv/pkg/shared/utils"
)

type userUsecase struct {
	userRepo interfaces.UserRepository
}

func NewUserUsecase(u interfaces.UserRepository) interfaces.UserUsecase {
	return &userUsecase{
		userRepo: u,
	}
}

func (u *userUsecase) Find() ([]entities.User, error) {
	users, err := u.userRepo.Find()
	return users, err
}

func (u *userUsecase) Create(user entities.User) (entities.User, error) {
	hashedPass, err := utils.HashPassword(user.Password)
	if err != nil {
		return user, err
	}
	user.Password = hashedPass
	user, err = u.userRepo.Create(user)
	return user, err
}

func (u *userUsecase) TakeByID(id uint) (entities.User, error) {
	user, err := u.userRepo.TakeByID(id)
	return user, err
}

func (u *userUsecase) TakeByUsername() (entities.User, error) {
	user := entities.User{}
	return user, nil
}

func (u *userUsecase) TakeByEmail() (entities.User, error) {
	user := entities.User{}
	return user, nil
}
