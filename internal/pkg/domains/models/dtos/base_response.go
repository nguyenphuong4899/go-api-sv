package dtos

type BaseResponse struct {
	Status string         `json:"status"`
	Data   interface{}    `json:"data"`
	Error  *ErrorResponse `json:"error"`
}

// ErrorResponse struct
type ErrorResponse struct {
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
}
