package entities

import "time"

// ProjectTableName ProjectName
var ProjectTableName = "projects"

type Project struct {
	BaseEntity
	Name              string     `gorm:"column:name;type:varchar(255);not null"`
	Category          string     `gorm:"column:category;type:varchar(255);not null;default='client'"`
	ProjectedSpend    int32      `gorm:"column:projected_spend;type:int;not null;default=0"`
	ProjectedVariance int32      `gorm:"column:projected_variance;type:int;not null;default=0"`
	RevenueRecognised int32      `gorm:"column:revenue_recognised;type:int;not null;default=0"`
	ProjectStartedAt  time.Time  `gorm:"column:project_started_at;type:datetime;not null"`
	ProjectEndedAt    *time.Time `gorm:"column:project_ended_at;type:datetime"`
}

func (i *Project) ProjectName() string {
	return ProjectTableName
}
