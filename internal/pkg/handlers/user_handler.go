package handlers

import (
	"errors"
	"go-api-sv/internal/pkg/domains/interfaces"
	"go-api-sv/internal/pkg/domains/models/dtos"
	"go-api-sv/internal/pkg/domains/models/entities"
	"go-api-sv/internal/pkg/repositories"
	"go-api-sv/internal/pkg/usecases"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

// UserHandler struct
type UserHandler struct {
	logger      *logrus.Logger
	UserUsecase interfaces.UserUsecase
}

// NewUserHandler func
func NewUserHandler(logger *logrus.Logger, dbConn *gorm.DB) *UserHandler {
	userRepo := repositories.NewUserRepository(dbConn)
	userUsecase := usecases.NewUserUsecase(userRepo)
	return &UserHandler{
		logger:      logger,
		UserUsecase: userUsecase,
	}
}

// GetUserList get all user
// HTTP Method: GET
// Params: none
func (h *UserHandler) GetUserList(c *gin.Context) {
	users, err := h.UserUsecase.Find()
	if err != nil {
		c.JSON(http.StatusInternalServerError, dtos.BaseResponse{
			Error: &dtos.ErrorResponse{
				ErrorMessage: err.Error(),
			},
		})
		return
	}

	userResp := []dtos.UserResponse{}
	for _, user := range users {
		userResp = append(userResp, convertUserEntityToUserResponse(user))
	}

	c.JSON(http.StatusOK, dtos.BaseResponse{
		Status: "success",
		Data:   userResp,
	})
}

// CreateUser create a user
// HTTP Method: POST
// Params:
// - username
// - email
// - password
func (h *UserHandler) CreateUser(c *gin.Context) {
	userReq := dtos.UserCreateRequest{}
	err := c.ShouldBindWith(&userReq, binding.Form)
	if err != nil {
		c.JSON(http.StatusBadRequest, dtos.BaseResponse{
			Status: "failed",
			Error: &dtos.ErrorResponse{
				ErrorMessage: err.Error(),
			},
		})
		return
	}

	user := entities.User{
		Username: userReq.Username,
		Email:    userReq.Email,
		Password: userReq.Password,
	}

	user, err = h.UserUsecase.Create(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dtos.BaseResponse{
			Status: "failed",
			Error: &dtos.ErrorResponse{
				ErrorMessage: err.Error(),
			},
		})
		return
	}

	c.JSON(http.StatusOK, dtos.BaseResponse{
		Status: "success",
		Data:   convertUserEntityToUserResponse(user),
	})
}

func (h *UserHandler) GetUserByID(c *gin.Context) {
	req := dtos.UserDetailRequest{}
	err := c.ShouldBindUri(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, dtos.BaseResponse{
			Status: "failed",
			Error: &dtos.ErrorResponse{
				ErrorMessage: err.Error(),
			},
		})
		return
	}

	user, err := h.UserUsecase.TakeByID(req.ID)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			c.JSON(http.StatusNotFound, dtos.BaseResponse{
				Status: "failed",
				Error: &dtos.ErrorResponse{
					ErrorMessage: err.Error(),
				},
			})
			return
		}

		c.JSON(http.StatusInternalServerError, dtos.BaseResponse{
			Status: "failed",
			Error: &dtos.ErrorResponse{
				ErrorMessage: err.Error(),
			},
		})
		return
	}
	c.JSON(http.StatusOK, dtos.BaseResponse{
		Status: "success",
		Data:   convertUserEntityToUserResponse(user),
	})
}

// convertUserEntityToUserResponse func
func convertUserEntityToUserResponse(user entities.User) dtos.UserResponse {
	return dtos.UserResponse{
		ID:        user.ID,
		Username:  user.Username,
		Email:     user.Email,
		CreatedAt: user.CreatedAt,
		UpdatedAt: user.UpdatedAt,
		DeletedAt: user.DeletedAt,
	}
}
