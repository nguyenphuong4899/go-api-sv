package router

import (
	"go-api-sv/internal/pkg/domains/models/dtos"
	"go-api-sv/internal/pkg/handlers"
	"go-api-sv/pkg/shared/middleware"
	"net/http"

	validator "go-api-sv/pkg/shared/validator"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

// Router is application struct
type Router struct {
	Engine *gin.Engine
	DBCon  *gorm.DB
	Logger *logrus.Logger
}

// InitializeRouter initializes Engine and middleware
func (r *Router) InitializeRouter(logger *logrus.Logger) {
	r.Engine.Use(gin.Logger())
	r.Engine.Use(gin.Recovery())
	r.Logger = logger
}

// SetupHandler set database and redis and usecase.
func (r *Router) SetupHandler() {
	userHandler := handlers.NewUserHandler(r.Logger, r.DBCon)
	_ = validator.New()

	// health check
	r.Engine.GET("/health_check", func(c *gin.Context) {
		data := dtos.BaseResponse{
			Status: "success",
			Data:   gin.H{"message": "Health check OK!"},
			Error:  nil,
		}
		c.JSON(http.StatusOK, data)
	})

	// router api
	api := r.Engine.Group("/api")
	{
		// user
		userAPI := api.Group("/user")
		{
			userAPI.GET("/list", userHandler.GetUserList)
			userAPI.POST("/create", userHandler.CreateUser)
			userAPI.GET(":id", userHandler.GetUserByID)
		}
		api.Use(middleware.CheckAuthentication())
		{
			api.GET("/test", func(c *gin.Context) {
				data := dtos.BaseResponse{
					Status: "success",
					Data:   gin.H{"message": "test"},
					Error:  nil,
				}
				c.JSON(http.StatusOK, data)
			})
		}

	}
}
